# WARNING!! : this is a BSD MAKEFILE and does NOT intend to be compatible with GNU make!

# C compiler stuff
CC = clang
CFLAGS = -ffreestanding -nostdlib -nostdinc -fno-builtin -fno-stack-protector -nodefaultlibs -target x86_64-none-elf

# output non-rust build stuff here
BUILD ?= ./build

# get the build mode
# defaults to debug but sets to release if the MODE env var is set
CARGO_FLAGS =
# default to debug mode
TARGET_MODE = debug
.if "${MODE}" == "release"
CARGO_FLAGS += "--release"
TARGET_MODE = release
.endif

# the output dir for cargo
TARGET = target/x86_64-untrust/${TARGET_MODE}

all: ${BUILD} ${BUILD}/libchalk.a ${BUILD}/untrust.bin

# set the display type for QEMU
# my mac didn't assume cocoa for some reason so I had to do this
OS = $$( uname -s )
DISPLAY ?= $$( \
	case $(OS) in \
		(Darwin) echo "cocoa" ;; \
		(*) echo "sdl" ;; \
	esac )

${BUILD}:
	mkdir -p ${BUILD}

${BUILD}/power.o: lib/chalk/power.c
	${CC} -c -o ${BUILD}/power.o ${CFLAGS} lib/chalk/power.c

${BUILD}/port.o: lib/chalk/port.c
	${CC} -c -o ${BUILD}/port.o ${CFLAGS} lib/chalk/port.c

${BUILD}/libchalk.a: ${BUILD}/power.o ${BUILD}/port.o
	llvm-ar rcs ${BUILD}/libchalk.a ${BUILD}/power.o ${BUILD}/port.o

${BUILD}/untrust.bin: ${BUILD}/libchalk.a src/**
	cargo bootimage ${CARGO_FLAGS}
	cp ${TARGET}/bootimage-untrust.bin ${BUILD}/untrust.bin

# build the toolchain by running sys/chain.sh
chain:
	sys/chain.sh

# run in a vm
run: all
	# but the bin in a nicer place
	cp ${TARGET}/bootimage-untrust.bin ${BUILD}/untrust.bin
	# create a hard disk image
	qemu-img create disk.img 1024
	# write "hello world" to the disk image
	# TODO: we shouldn't have to do this lol
	r2 -w -c "w hello world" -q disk.img
	# run qemu
	# options:
	#  - 2048M of memory
	#  - display of type ${DISPLAY}
	#  - add a disk drive from disk.img
	#  - user defined ${QEMU_FLAGS}
	qemu-system-x86_64 -serial stdio \
		-machine pc \
		-m size=2048 \
		${BUILD}/untrust.bin \
		-display ${DISPLAY} \
		-device ahci,id=ahci \
		-drive file=disk.img,id=disk,if=none,format=raw \
		-device ide-hd,drive=disk,bus=ahci.0 \
		${QEMU_FLAGS}

# clean out the directory
clean:
	cargo clean
	rm -rf ${BUILD} disk.img

# format our code
fmt:
	cargo fmt
	clang-format -i lib/chalk/*.c lib/chalk/*.h

# just check for compile errors without actually compiling
check:
	cargo check

# read The Holy Bible
read-doc:
	man sys/doc/untrust.7

# a nice little help message :)
help:
	@echo "UNTRUST Makefile help"
	@echo ""
	@echo "CONFIGURATION:"
	@echo "    MODE            sets the cargo build mode. either release or debug"
	@echo "    QEMU_FLAGS      adds flags to the qemu command in \"make run\""
	@echo "    DISPLAY         set the \"display\" flag for qemu"
	@echo "    CC              set the C compiler to use (defaults to clang)"
	@echo "    CFLAGS          set the C compiler flags"
	@echo "    BUILD           set the build directory for non-rust outputs WARNING! THIS COULD VERY EASILY BREAK LINKAGE IN THE RUST COMPILER"
	@echo ""
	@echo "TARGETS:"
	@echo "    chain           runs \"sys/chain.sh\" to build the toolchain"
	@echo "    run             builds a bootable image and runs it in qemu"
	@echo "    clean           cleans out compiled binaries and other build output"
	@echo "    check           just runs \"cargo check\" to save time when testing"
	@echo "    fmt             formats your code. nice for pull requests :-)"
	@echo "    read-doc        read the untrust structure document"
