use crate::{
    ahci::{
        ahci_sys::{HBA_MEM, HBA_PORT},
        probe,
    },
    ata::{read, write},
    chalk::reboot,
    dbg, dbgln, log, print, println,
};
use alloc::str;
use alloc::string::{String, ToString};
use conquer_once::spin::OnceCell;
use core::{
    pin::Pin,
    task::{Context, Poll},
};
use crossbeam_queue::ArrayQueue;
use futures_util::{
    stream::{Stream, StreamExt},
    task::AtomicWaker,
};
use lazy_static::lazy_static;
use pc_keyboard::{layouts, DecodedKey, HandleControl, KeyCode, Keyboard, ScancodeSet1};
use spin::Mutex;

static SCANCODE_QUEUE: OnceCell<ArrayQueue<u8>> = OnceCell::uninit();
static WAKER: AtomicWaker = AtomicWaker::new();

/// Called by the keyboard interrupt handler
///
/// Must not block or allocate.
pub(crate) fn add_scancode(scancode: u8) {
    if let Ok(queue) = SCANCODE_QUEUE.try_get() {
        if let Err(_) = queue.push(scancode) {
            println!("WARNING: scancode queue full; dropping keyboard input");
        } else {
            WAKER.wake();
        }
    } else {
        println!("WARNING: scancode queue uninitialized");
    }
}

pub struct ScancodeStream {
    _private: (),
}

impl ScancodeStream {
    pub fn new() -> Self {
        SCANCODE_QUEUE
            .try_init_once(|| ArrayQueue::new(100))
            .expect("ScancodeStream::new should only be called once");
        ScancodeStream { _private: () }
    }
}

impl Stream for ScancodeStream {
    type Item = u8;

    fn poll_next(self: Pin<&mut Self>, cx: &mut Context) -> Poll<Option<u8>> {
        let queue = SCANCODE_QUEUE
            .try_get()
            .expect("scancode queue not initialized");

        // fast path
        if let Ok(scancode) = queue.pop() {
            return Poll::Ready(Some(scancode));
        }

        WAKER.register(&cx.waker());
        match queue.pop() {
            Ok(scancode) => {
                WAKER.take();
                Poll::Ready(Some(scancode))
            }
            Err(crossbeam_queue::PopError) => Poll::Pending,
        }
    }
}

lazy_static! {
    pub static ref KEY_BUFFER: Mutex<String> = Mutex::new(String::new());
}

/// temporary "shell"
unsafe fn exec_command() {
    match (*KEY_BUFFER.lock()).as_str() {
        "hello" => println!("Hello, world!"),
        "read" => println!("{:?}", read(0, 1024)),
        "write" => write(0, b"Hello, world!", 1024),
        "write2" => write(0, b"lolololol", 1024),
        "read_utf" => println!("{:?}", str::from_utf8(&read(0, 1024))),
        "probe" => {
            log! {
                "Getting HBA_MEM from probe" => unsafe {
                    match probe() {
                        Ok(bar) => {
                            dbgln!("found abar at: {}", bar);
                            let mut mem = &mut *(bar as *mut HBA_MEM);
                            dbgln!("port: {:?}", mem.ports[0]);
                        },
                        Err(e) => println!("[ERROR]: {:?}", e)
                    };
                }
            };
        },
        "reboot" => reboot(),
        "help"|"?" => {
            println!("UNTRUST COMMAND MANUAL:");
            println!("\t  help|?     print this message");
            println!("\t  hello      you already know what this does");
            println!("\t! read       read with ATA");
            println!("\t! write      write with ATA");
            println!("\t! write2     write something else with ATA");
            println!("\t! read_utf   read from ATA as UTF-8");
            println!("\t  probe      probe AHCI devices");
            println!("\t! reboot     reboot");
            println!("\nCommands marked with \"!\" have a high likelyhood of hanging the kernel.");
            println!("For additional questions or concerns, please email me at skylarbleed@protonmail.ch");
        },
        c => println!("ERROR: command \"{}\" not found", c),
    }
    *KEY_BUFFER.lock() = String::new();
    print!("\n> ");
}

pub async fn print_keypresses() {
    let mut scancodes = ScancodeStream::new();
    let mut keyboard = Keyboard::new(layouts::Us104Key, ScancodeSet1, HandleControl::Ignore);

    while let Some(scancode) = scancodes.next().await {
        if let Ok(Some(key_event)) = keyboard.add_byte(scancode) {
            if let Some(key) = keyboard.process_keyevent(key_event) {
                match key {
                    DecodedKey::Unicode(character) => {
                        print!("{}", character);
                        unsafe {
                            match character {
                                '\n' => exec_command(),
                                '\x08' => {
                                    (*KEY_BUFFER.lock()).pop();
                                    ()
                                }
                                _ => *KEY_BUFFER.lock() += character.to_string().as_str(),
                            }
                        }
                    }
                    DecodedKey::RawKey(key) => match key {
                        KeyCode::Backspace | KeyCode::Delete => {
                            (*KEY_BUFFER.lock()).pop();
                            ()
                        }
                        _ => print!("{:?}", key),
                    },
                }
            }
        }
    }
}
