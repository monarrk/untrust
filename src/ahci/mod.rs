#[allow(dead_code)]
#[allow(non_camel_case_types)]
#[allow(non_snake_case)]
pub mod ahci_sys;

use crate::dbgln;
use crate::port::{In, Out};
use ahci_sys::{FIS_REG_H2D, FIS_TYPE_REG_H2D, HBA_CMD_HEADER, HBA_CMD_TBL, HBA_MEM, HBA_PORT};

use alloc::{string::String, vec::Vec};
use core::mem::size_of;
use num_traits::AsPrimitive;

#[derive(Debug)]
pub enum AhciError {
    ReadError(String),
    CmdSlotNotFound,
    PortIsHung,
    NoControllerFound,
    EIO,
}

macro_rules! pci_config_read {
    ($t:ty, $bus:expr, $slot:expr, $func:expr, $offset:expr) => {{
        let address = ((($bus << 16) | ($slot << 11)) as u32
            | ($func << 8) as u32
            | ($offset & 0xfc) as u32
            | 0x80000000u32) as u32;
        // dbgln!("pci address: {}", address);

        // write the address
        unsafe { u32::io_out(0xCF8, address) };
    }};
}

fn pci_config_read_dword(bus: u16, slot: u16, func: u16, offset: u16) -> u32 {
    pci_config_read!(u32, bus as u32, slot as u32, func as u32, offset as u32);

    // read the data
    unsafe { u32::io_in(0xCFC) } // >> ((offset & 2) * 8)) & 0xffff)
}

fn pci_config_read_word(bus: u16, slot: u16, func: u16, offset: u16) -> u16 {
    pci_config_read!(u16, bus as u32, slot as u32, func as u32, offset as u32);

    // read the data
    unsafe { ((u32::io_in(0xCFC) >> (((offset as u32) & 3) * 8)) & 0xffff) as u16 }
}

/// find an ahci device through brute force
pub fn probe() -> Result<u64, AhciError> {
    // 256 buses
    for bus in 0..256 {
        // 32 devices per bus
        for slot in 0..32 {
            let vendor = pci_config_read_word(bus, slot, 0, (0x00 | 0x0));
            let device = pci_config_read_word(bus, slot, 0, (0x00 | 0x02));
            if device != 0xFFFF && vendor != 0xFFFF {
                let class_n = pci_config_read_dword(bus, slot, 0, 0x08);
                let class = (((1 << 7) - 1) & (class_n >> (24 - 1)));
                let subclass = (((1 << 7) - 1) & (class_n >> (16 - 1)));
//                dbgln!("class_n: {}, class: {}, subclass: {}", class_n, class, subclass);
                if class == 0x01 && subclass == 0x06 {
                    let config = pci_config_read_dword(bus, slot, 0, (0x3c | 0x0));
                    // extract bytes 4 to 31
                    let bar_addr = (((1 << 27) - 1) & (config >> (4 - 1))) as u64;
                    return Ok(bar_addr);
                }
            }
        }
    }

    Err(AhciError::NoControllerFound)
}

const HBA_PXIS_TFES: u32 = (1 << 30);

fn find_cmdslot(port: &mut HBA_PORT) -> Result<u32, AhciError> {
    let mut slots = port.sact | port.ci;

    // TODO how do we find the number of cmdslots??
    for i in 0..3 {
        if (slots & 1) == 0 {
            return Ok(i);
        }
        slots >>= 1;
    }

    Err(AhciError::CmdSlotNotFound)
}

pub fn read(
    port: &mut HBA_PORT,
    startl: u32,
    starth: u32,
    count: u32,
) -> Result<Vec<u16>, AhciError> {
    let mut count = count;

    // clear pending bits
    port.is = 1u32;

    let mut spin = 0;

    // get slot
    let slot = match find_cmdslot(port) {
        Ok(s) => s,
        Err(e) => return Err(e),
    };

    let mut cmdheader = unsafe { &mut *((port.clb + slot) as *mut HBA_CMD_HEADER) };

    // command FIS size
    cmdheader.set_cfl((size_of::<FIS_REG_H2D>() / size_of::<u32>()) as u8);

    // read from device
    cmdheader.set_w(0);

    // PRDT entries count
    cmdheader.prdtl = ((count - 1) >> 4) as u16;

    // get the command table
    let mut cmdtbl = unsafe { &mut *(cmdheader.ctba as *mut HBA_CMD_TBL) };

    let buf = [0u16; 8 * 1024];

    let mut idx = 0;
    // 8K bytes (16 sectors) per PRDT
    for i in 0..(cmdheader.prdtl as usize - 1usize) {
        // TODO: buf is probably broken here
        // translating pointer arithmetic is hard
        cmdtbl.prdt_entry[i].dba = (&buf) as *const _ as u32;
        cmdtbl.prdt_entry[i].set_dbc((8 * 1024) - 1);
        cmdtbl.prdt_entry[i].set_i(1);

        // 4K words
        idx += 4 * 1024;

        // 16 sectors
        count -= 16;
    }

    // last entry
    cmdtbl.prdt_entry[cmdheader.prdtl as usize].dba = (&buf) as *const _ as u32;
    cmdtbl.prdt_entry[cmdheader.prdtl as usize].set_dbc((count << 9) - 1);
    cmdtbl.prdt_entry[cmdheader.prdtl as usize].set_i(1);

    // setup command
    let mut cmdfis = unsafe { &mut *(cmdtbl.cfis.as_ptr() as *mut FIS_REG_H2D) };
    cmdfis.fis_type = FIS_TYPE_REG_H2D as u8;
    cmdfis.set_c(1);
    // TODO ATA_CMD_READ_DMA_EX is not defined, so I'm guessing that it's 25
    cmdfis.command = 25;

    cmdfis.lba0 = startl as u8;
    cmdfis.lba1 = (startl >> 8) as u8;
    cmdfis.lba2 = (startl >> 16) as u8;
    // lba mode
    cmdfis.device = 1 << 6;

    cmdfis.lba3 = (startl >> 24) as u8;
    cmdfis.lba4 = starth as u8;
    cmdfis.lba5 = (starth >> 8) as u8;

    cmdfis.countl = (count & 0xff) as u8;
    cmdfis.counth = ((count >> 8) & 0xff) as u8;

    // wait until port is no longer busy
    while (port.tfd & (0x80 | 0x08)) != 0 && spin < 1_000_000 {
        spin += 1;
    }

    if spin == 1_000_000 {
        return Err(AhciError::PortIsHung);
    }

    // issue command
    port.ci = 1 << slot;

    // wait for completion
    loop {
        if (port.ci & (1 << slot)) == 0 {
            break;
        }

        if port.is & HBA_PXIS_TFES != 0 {
            return Err(AhciError::ReadError(String::from("Read disk error")));
        }
    }

    // check again
    if port.is & HBA_PXIS_TFES != 0 {
        return Err(AhciError::ReadError(String::from("Read disk error")));
    }

    Ok(Vec::from(buf))
}
