// C type conversions
// mostly for bindgen

pub type c_uchar = u8;
pub type c_ushort = u16;
pub type c_uint = u32;
pub type c_ulonglong = u64;
