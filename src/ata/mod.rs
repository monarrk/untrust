use crate::port::{In, Out};
use crate::{dbg, dbgln, log};
use alloc::string::String;

const ATA_BASE0: u16 = 0x1F0;
const ATA_BASE1: u16 = 0x170;
const ATA_BASE: [u16; 4] = [ATA_BASE0, ATA_BASE0, ATA_BASE1, ATA_BASE1];

// ata registers
const ATA_DATA: u16 = 0;
const ATA_COUNT: u16 = 2;
const ATA_SECTOR: u16 = 3;
const ATA_CYL_LO: u16 = 4;
const ATA_CYL_HI: u16 = 5;
const ATA_FDH: u16 = 6;
const ATA_STATUS: u16 = 7;
const ATA_COMMAND: u16 = 7;
const ATA_CONTROL: u16 = 0x206;

// https://github.com/dthain/basekernel/blob/master/kernel/ata.c

// TODO: make dynamic
const SIZE: usize = 512;

macro_rules! pio_commands {
    ($base:expr, $cmd:expr, $offset:expr) => {
        log! {
            // flags
            "Sending flags" => u8::io_out($base + ATA_FDH, 0x80 | 0x40 | 0x20)
            // null byte
            // TODO not needed
            "Sending 0x00 to ATA_CTRL" => u8::io_out($base + ATA_CONTROL, 0x00)
            // block count
            "Sending block count" => u8::io_out($base + ATA_COUNT, SIZE)
            // sector size
            "Sending sector size" => u8::io_out($base + ATA_SECTOR, 0)
            // low 8 bits of LBA
            "Sending low 8 bits of LBA" => u8::io_out($base + ATA_CYL_LO, ($offset >> 8u8) & 0xFF)
            // hi 8 bits of LBA
            "Sending high 8 bits of LBA" => u8::io_out($base + ATA_CYL_HI, ($offset >> 16u8) & 0xFF)
            // send the command (0x20 for read)
            "Sending command" => u8::io_out($base + ATA_COMMAND, $cmd)
            // wait
            "Waiting for BSY" => bsy($base)
            "Waiting for DRQ" => drq($base)
        }
    };
}

/// wait for BSY flag to be unset
unsafe fn bsy(base: u16) {
    log! {
        "Waiting" => while u8::io_in(base + ATA_STATUS) & 0x80 == 0 { }
    }
}

/// wait for DRQ to be ready
unsafe fn drq(base: u16) {
    log! {
        "Waiting" => while u8::io_in(base + 0x08) & 0x08 == 0x08 { }
    }
}

pub unsafe fn identify(id: usize) -> [u8; SIZE] {
    pio_commands!(id as u16, 0xec, 512);

    let mut v = [0u8; SIZE];
    log! {
        "Reading" => for i in (0..SIZE).step_by(2) {
            let [a, b] = u16::io_in(ATA_BASE[id] + ATA_DATA).to_be_bytes();
            v[i] = a;
            if i < v.len() {
                v[i + 1] = b;
            }
        }
    }

    v
}

pub unsafe fn probe(id: usize) -> Result<(), String> {
    // check for 0xFF in status register which means nothing is attached
    log! {
        "Checking if drive is attached" => if u8::io_in(ATA_BASE[id] + ATA_STATUS) == 0xFF {
            return Err(String::from("No drive attached!"));
        }
    }

    let buffer = identify(id);
    dbgln!("{:?}", buffer);

    Ok(())
}

/// read SIZE bytes
pub unsafe fn read(id: usize, offset: u32) -> [u8; SIZE] {
    pio_commands!(id as u16, 0x20, offset);

    let mut v = [0u8; SIZE];
    log! {
        "Reading" => for i in (0..SIZE).step_by(2) {
            let [a, b] = u16::io_in(ATA_BASE[id] + ATA_DATA).to_be_bytes();
            v[i] = a;

            // this should never be false but Just In Case
            if i < v.len() {
                v[i + 1] = b;
            }
        }
    }

    v
}

/// write a buffer
pub unsafe fn write(id: usize, buffer: &[u8], offset: u32) {
    pio_commands!(id as u16, 0x30, offset);

    log! {
        "Writing" => for i in (0..buffer.len()).step_by(2) {
            let n = ((buffer[i] as u16) << 8) | if (i + 1) < buffer.len() {
                buffer[i + 1] as u16
            } else {
                0x00u16
            };
            u16::io_out(ATA_BASE[id] + ATA_DATA, n)
        }
    }
}
