use crate::chalk::{inb, inl, inw, outb, outl, outw};
use num_traits::cast::AsPrimitive;

pub trait Out {
    unsafe fn io_out<T: AsPrimitive<u16>, E: AsPrimitive<Self>>(port: T, value: E)
    where
        Self: 'static + core::marker::Copy;
}

pub trait In {
    unsafe fn io_in<T: AsPrimitive<u16>>(port: T) -> Self;
}

impl Out for u8 {
    unsafe fn io_out<T: AsPrimitive<u16>, E: AsPrimitive<Self>>(port: T, value: E) {
        outb(port.as_(), value.as_());
    }
}

impl Out for u16 {
    unsafe fn io_out<T: AsPrimitive<u16>, E: AsPrimitive<Self>>(port: T, value: E) {
        outw(port.as_(), value.as_());
    }
}

impl Out for u32 {
    unsafe fn io_out<T: AsPrimitive<u16>, E: AsPrimitive<Self>>(port: T, value: E) {
        outl(port.as_(), value.as_());
    }
}

impl In for u8 {
    unsafe fn io_in<T: AsPrimitive<u16>>(port: T) -> u8 {
        inb(port.as_())
    }
}

impl In for u16 {
    unsafe fn io_in<T: AsPrimitive<u16>>(port: T) -> u16 {
        inw(port.as_())
    }
}

impl In for u32 {
    unsafe fn io_in<T: AsPrimitive<u16>>(port: T) -> u32 {
        inl(port.as_())
    }
}
