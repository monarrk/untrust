#![no_std]
#![no_main]
#![feature(custom_test_frameworks)]
#![test_runner(blog_os::test_runner)]
#![reexport_test_harness_main = "test_main"]

extern crate alloc;

use bootloader::{entry_point, BootInfo};
use core::panic::PanicInfo;
use untrust::task::{executor::Executor, keyboard, Task};
use untrust::{
    ahci::{
        self,
        ahci_sys::{HBA_MEM, HBA_PORT},
        probe,
    },
    dbg, dbgln, log, print, println,
};

entry_point!(kernel_main);

fn kernel_main(boot_info: &'static BootInfo) -> ! {
    use untrust::allocator;
    use untrust::memory::{self, BootInfoFrameAllocator};
    use x86_64::VirtAddr;

    println!("Welcome to UntrustOS!\n\t-> \"Better than nothing!\"\n");
    println!("Enter `?` for help");
    untrust::init();

    let phys_mem_offset = VirtAddr::new(boot_info.physical_memory_offset);
    let mut mapper = unsafe { memory::init(phys_mem_offset) };
    let mut frame_allocator = unsafe { BootInfoFrameAllocator::init(&boot_info.memory_map) };

    allocator::init_heap(&mut mapper, &mut frame_allocator).expect("heap initialization failed");

    let mut executor = Executor::new();
    executor.spawn(Task::new(keyboard::print_keypresses()));

    print!("\n> ");
    executor.run();
}

/// This function is called on panic.
#[cfg(not(test))]
#[panic_handler]
fn panic(info: &PanicInfo) -> ! {
    println!("{}", info);
    untrust::hlt_loop();
}

#[test_case]
fn trivial_assertion() {
    assert_eq!(1, 1);
}
