#ifndef CHALK_PORT_H
#define CHALK_PORT_H

unsigned char inb(int port);
unsigned short inw(int port);
unsigned short inl(int port);
void outb(unsigned char value, int port);
void outw(unsigned short value, int port);
void outl(unsigned int value, int port);
void iowait();
unsigned char inb_slow(int port);
unsigned short inw_slow(int port);
unsigned int inl_slow(int port);
void outb_slow(unsigned char value, int port);
void outw_slow(unsigned short value, int port);
void outl_slow(unsigned int value, int port);

#endif
