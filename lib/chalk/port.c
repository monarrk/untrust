#include "port.h"

unsigned char inb(int port) {
	unsigned char result;
	asm("inb %w1, %b0"
		: "=a"(result)
		: "Nd"(port));
	return result;
}

unsigned short inw(int port) {
	unsigned short result;
	asm("inw %w1, %w0"
		: "=a"(result)
		: "Nd"(port));
	return result;
}

unsigned short inl(int port) {
	unsigned int result;
	asm("inl %w1, %0"
		: "=a"(result)
		: "Nd"(port));
	return result;
}

void outb(unsigned char value, int port) {
	asm("outb %b0, %w1"
		:
		: "a"(value), "Nd"(port));
}

void outw(unsigned short value, int port) {
	asm("outw %w0, %w1"
		:
		: "a"(value), "Nd"(port));
}

void outl(unsigned int value, int port) {
	asm("outl %0, %w1"
		:
		: "a"(value), "Nd"(port));
}

void iowait() {
	outb(0, 0x80);
}

unsigned char inb_slow(int port) {
	unsigned char result = inb(port);
	iowait();
	return result;
}

unsigned short inw_slow(int port) {
	unsigned short result = inw(port);
	iowait();
	return result;
}

unsigned int inl_slow(int port) {
	unsigned int result = inl(port);
	iowait();
	return result;
}

void outb_slow(unsigned char value, int port) {
	outb(value, port);
	iowait();
}

void outw_slow(unsigned short value, int port) {
	outw(value, port);
	iowait();
}

void outl_slow(unsigned int value, int port) {
	outl(value, port);
	iowait();
}
