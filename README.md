# untrust
experimental operating system: try 2

<img src="/sys/shots/hello.png" alt="Hello world screen shot :)"/>

## building
This project depends on
- [BSD Make](https://wiki.netbsd.org/tutorials/bsd_make/). this project is NOT compatible with GNU make, nor does it plan to be 
- [rust](https://rust-lang.org) note: MUST be nightly!
- [clang](https://clang.llvm.org/)
- qemu (for testing)

rust and other build dependencies not listed here are installed automatically for you by `sys/chain.sh` or `make chain`.

### TL;DR
run this:

```
MODE=release make chain all
```

If you are interested in learning more about the different configuration options or Makefile targets, run `make help`.

## structure
You can find a troff document (readable with man) describing the structure of this project called `sys/doc/untrust.7`. This has the same general purpose as an ARCHITECTURE.md file. You can also view it with `make read-doc`, but manual commands can differ system to system so this might not work on your machine. If you intend to help work on this project, that's the place to start.

## TODOs
this list might become outdated quickly, so beware!
- ATA/AHCI driver
- Flat file system implementation
- Mouse driver
- driver translation into the libchalk libOS

## credit
- [Phil Opperman](https://os.phil-opp.com/) for the tutorial this was originally based on
- [RKennedy9064](https://gist.github.com/RKennedy9064)'s mouse driver
